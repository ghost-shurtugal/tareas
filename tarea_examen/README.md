Tarea Examen 4
----------------------------------------------------
<p>
	<img src="img/Diagrama.PNG"/>
</p>
Implementada por Josué Rodrigo Cárdenas Vallarta

## Procedimineto

- Se hizo el escenario con Packet tracer tal cuál se muestra en la imagen de ejemplo en la página del proyecto.
- Se definieron las redes a partir de la siguiente sustitución:<br/>
 <b>A</b> por <b>1</b>, <b>B</b> por <b>2</b>, <b>C</b> por <b>3</b>, <b>X</b> por <b>4</b>, <b>Y</b> por <b>5</b> y <b>Z</b> por <b>6</b>.
- Una vez definidas las redes se pasó a configurar los routers con los comandos ubicados en el archivo <a href='config.txt'>config.txt</a>, una vez ejecutados y guardados en su configuración se obtuvo lo sig:
<br/><br/>
<p>
	<b>Router0</b><br/>
	<a href="img/conf-router-Router0-0.PNG">conf-router-Router0-0</a><br/>
	<a href="img/conf-router-Router0-1.PNG">conf-router-Router0-1</a><br/>
	<a href="img/conf-router-Router0-2.PNG">conf-router-Router0-2</a><br/>
	<a href="img/conf-router-Router0-3.PNG">conf-router-Router0-3</a><br/>
</p>
<p>
	<b>Router1</b><br/>
	<a href="img/conf-router-Router1-0.PNG">conf-router-Router1-0</a><br/>
	<a href="img/conf-router-Router1-1.PNG">conf-router-Router1-1</a><br/>
	<a href="img/conf-router-Router1-2.PNG">conf-router-Router1-2</a><br/>
	<a href="img/conf-router-Router1-3.PNG">conf-router-Router1-3</a><br/>
</p>
<p>
	<b>Router2</b><br/>
	<a href="img/conf-router-Router2-0.PNG">conf-router-Router2-0</a><br/>
	<a href="img/conf-router-Router2-1.PNG">conf-router-Router2-1</a><br/>
	<a href="img/conf-router-Router2-2.PNG">conf-router-Router2-2</a><br/>
	<a href="img/conf-router-Router2-3.PNG">conf-router-Router2-3</a><br/>
</p>
- Acto seguido se comenzarona  configurar los servidores con la ayuda da la interfaz de Packet Tracer, el resultado fue el siguiente:
<br/><br/>
<p>
	<b>Server0-DNS</b><br/>
	<a href="img/conf-servidor-Server0-DNS-1.PNG">conf-servidor-Server0-DNS-1</a><br/>
	<a href="img/conf-servidor-Server0-DNS-2.PNG">conf-servidor-Server0-DNS-2</a><br/>
</p>
<p>
	<b>Server1-HTTP</b><br/>
	<a href="img/conf-servidor-Server1-HTTP-1.PNG">conf-servidor-Server1-HTTP-1</a><br/>
	<a href="img/conf-servidor-Server1-HTTP-2.PNG">conf-servidor-Server1-HTTP-2</a><br/>
	<a href="img/conf-servidor-Server1-HTTP-3.PNG">conf-servidor-Server1-HTTP-3</a><br/>
</p>
<p>
	<b>Server2-DHCP</b><br/>
	<a href="img/conf-servidor-Server2-DHCP-1.PNG">conf-servidor-Server2-DHCP-1</a><br/>
	<a href="img/conf-servidor-Server2-DHCP-2.PNG">conf-servidor-Server2-DHCP-2</a><br/>
</p>
- Una vez configurados los routers y servidores sólo quedaba configurar los equipos para que obtuvieran su IP por DHCP y los impresoras para que tuvieran su IP estática. Se obtuvo lo siguiente:
<br/><br/>
<p>
	<b>Pc0</b><br/>
	<a href="img/conf-cliente-PC0.PNG">conf-cliente-PC0</a><br/>
</p>
<p>
	<b>Laptop0</b><br/>
	<a href="img/conf-cliente-Laptop0.PNG">conf-cliente-Laptop0</a><br/>
</p>
<p>
	<b>Printer0</b><br/>
	<a href="img/conf-cliente-printer0.PNG">conf-cliente-printer0</a><br/>
</p>
<p>
	<b>Printer1</b><br/>
	<a href="img/conf-cliente-printer1.PNG">conf-cliente-printer1</a><br/>
</p>
- Después de hacer varias pruebas y algunos arreglos se hizo el sig. documento redactando el caso en el que un cliente se conecta al servidor web, dicho documento tiene lujo de detalle de la prueba completa:
<br/><br/>
<p>
	<b>Caso de petición de la página web desde un cliente</b><br/>
	<a href="Caso de petición de página web.pdf">Caso de petición de página web</a><br/>
</p>

Plantilla de captura de tráfico
----------------------------------------------------
- Se anexa la plantilla con las substituciones adecuadas en el siguiente enlace: <br/><br/>
<p>
	<b>Plantilla de tráfico</b><br/>
	<a href="examen.txt">examen.txt</a><br/>
</p>

Checksum
----------------------------------------------------
- Utilizando como ejemplo la captura hecha en la tarea de TCP (se encuentra en el archivo de <a href="captura-ejemplo.pcap.txt" >captura_ejemplo.pcap</a>) se logra observar que se ocupa el comando <b>cksum</b>, donde se aplica el algoritmo visto en clase para el protocolo y, a partir del resultado, se confirma si está correcto; en caso contrario manda error:
<br/>
<br/>
<b>Ejemplo  de error TCP</b>
<br/>
1  16:46:00.886202 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 76: (tos 0x0, ttl 64, id 12146, offset 0, flags [DF], proto TCP (6), length 60)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [S], <b>cksum 0x9fe1 (incorrect -> 0xf3ae)</b>, seq 1195242701, win 29200, options [mss 1460,sackOK,TS val 2888987593 ecr 0,nop,wscale 7], length 0
<br/>
<br/>
<b>Ejemplo de éxito TCP</b>
<br/>
5  16:46:01.319239  In a8:ad:3d:e5:01:64 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 53, id 46238, offset 0, flags [DF], proto TCP (6), length 52)
    198.49.23.145.http > 192.168.1.72.58934: Flags [.], <b>cksum 0xd272 (correct)</b>, ack 78, win 50, options [nop,nop,TS val 3284135439 ecr 2888987821], length 0
