HTTPS
----------------------------------------------------

* Creamos los archivos del certificado:
<p>
	<img src="img/1.png"/>
</p>
* Con la herramienta <b>keytool</b> de java importamos el certificado en un formato que el servidor de Glassfish pueda leer.
<p>
	<img src="img/2.png"/>
</p>
* Importamos la llave al archivo del servidor de Glassfish:
<p>
	<img src="img/3.png"/>
</p>
* En el menú del panel de administración de Glassifsh en la parte de configurations->server-config->HTTP Services->Http Listeners->http-listeners2 especificamos el puerto donde llevará la conexión segura.
<p>
	<img src="img/4.png"/>
</p>
* Especificamos el nombre del store donde se encuentra nuestro certificado.
<p>
	<img src="img/5.png"/>
</p>
* También reemplazamos en el archivo <b>/opt/glassfish4/glassfish/domains/domain1/config/domain.xml</b> todo atributo cert-nickname el valor que está por el que le pusimos al store.<br/> Reiniciamos el servidor y obtendremos la conexión segura.
<p>
	<img src="img/6.png"/>
</p>
