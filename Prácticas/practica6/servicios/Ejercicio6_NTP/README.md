NTP
----------------------------------------------------

* Instalamos la paquetería para el servicio.
<p>
	<img src="img/1.png"/>
</p>
* Configuramos el servicio para que tenga la hora adecuada y habilitamos el servicio para su uso.
<p>
	<img src="img/2.png"/>
</p>
* En caso de que tuvieramos más servidores y quisiéramos agregar direcciones o ips para que puedan acceder a este servicio el en archivo <b>/etc/ntp.conf</b> se agregan en la sección de <b># Hosts on local network are less restricted.</b> (reiniciamos el servicio después de actualizar el archivo).<br/>
Desde el cliente podremos sincronizarlo con el sig. comando:
<b>ntpdate -u <ip o host donde está el servidor NTP> </b>
