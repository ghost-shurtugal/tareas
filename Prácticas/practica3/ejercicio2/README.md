Ejercicio 2
----------------------------------------------------

## Public Key Infrastructure (PKI)

Consiste de combinar hardware, software políticas y estándares para administar la creación, administración, distribución y revocación de llaves y certificados digitales. Los certificados son el nucleo de la PKI de manera que afirman la identidad del sujeto del certificado y enlacan esa identidad a la llave pública contenida en el certificado.

* Elementos
  - Trusted party, llamado la autoridad certificada (CA), actúa como la raíz de confianza y provee de servicios que autentifican la identidad de individuos,computadores y otras entidades.
  - Autoridad de registro, comunmente llamado una CA subordinada, certificada por la CA raiz para certificar usos específicos por el permiso de la raíz.
  - Una base de datos de certificados, almacena peticiones de certificados, algunos asuntos y la eliminación de certificados.
  - Un almacén de certificados, reside en una computadora local y almacena algunas cuestiones de los certificados y llaves privadas.

La PKI provee la identificación de llaves públicas y su distribución, los provee a entidades e individuos después de verificar su identidad firmándolos con una llave privada.

Algunos de sus usos son:
* Identificación de usuarios y sistemas (login)
* Identificación del interlocutor
* Cifrado de datos digitales
* Firma Digital de datos (documentos, software, etc.)
* Asegurar las comunicaciones
* Garantía de no repudio (negar que cierta transacción tuvo lugar)


PKI tiene el problema de que, al ser una cadena de confianza, es tan fuerte como su eslabón más debil.

## Recursos
* PKI (public key infrastructure):
  - <a href="http://searchsecurity.techtarget.com/definition/PKI">Enlace 1</a>
  - <a href="https://www.tutorialspoint.com/cryptography/public_key_infrastructure.htm">Enlace 2</a>

