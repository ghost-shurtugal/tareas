Ejercicio 4
----------------------------------------------------

## 1. Comprobando el tráfico del puerto 443.

* Con el comando 'tcpdump -XAvi et0 port 443' obtenemos el siguiente resultado.

<p align="center">
  <img src="img/1.png"/>
</p>

Este se puede apreciar en el archivo salida_ejercicio4_ssl.txt
