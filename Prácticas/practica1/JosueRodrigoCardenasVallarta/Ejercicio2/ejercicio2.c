/**
* @file practica1.c
* @brief Este archivo contiene el ejercicio 2 de la práctica 1.
*
* @author Josué Rodrigo Cárdenas Vallarta
*
* @date 14/02/2018
*/

#include <stdio.h>


//Ejercicio 2

/**
 * @brief Función que imprime elementos de un arreglo de caracteres junto con su dirección en memoria.
 *
 * @param elementos Es el apuntador del arreglo de caracteres a imprimir.
 * @param longitudA Es el tamaño del arreglo.
*/
void printDirection(char* elementos, int longitudA){
	int n;
	printf("[");
	for(n=0;n<longitudA;n++){
		printf("%p=%c,",elementos[n],(char)elementos[n]);
	}
	printf("]\n");
}

/**
 * @brief Función que hace un swap de valores tipo caracter entre dos apuntdores de memoria.
 *
 * @param c1 Es el apuntador del caracter uno.
 * @param c2 Es el apuntador del caracter dos.
*/
void swap(char* c1, char* c2){
  char tmp=*c1;
  //Vamos a la localidad a la que apunta cada apuntador y le swapeamos el valor
  *c1=*c2;
  *c2=tmp;
}


/**
 * @brief Función que genera la partición para el algoritmo de quickSort y devuelve el índice
 *			del elemento pivote.
 *
 * @param elementos Es el apuntador del arreglo de caracteres.
 * @param desde Es el punto inicial de la partición.
 * @param hasta Es el punto final de la partición.
 *
 * @return El índice del elemento pivote.
 */
int particion(char* elementos, int desde, int hasta){
  char pivote = elementos[hasta];
  int indice = (desde - 1),n;
  for (n = desde; n <= hasta - 1; n++){
    if (elementos[n] <= pivote){
      indice++;
      swap(&elementos[indice], &elementos[n]);
    }
  }
  //Ahora cambiamos las direcciones en memoria
  swap(&elementos[indice + 1], &elementos[hasta]);
  return (indice + 1);
}

/**
 * @brief Función que implementa el algoritmo de quicksort, no genera un nuevo arreglo si no
 * 			que opera en el mismo arreglo que se le pasa como parámetro.
 *
 * @param elementos Es el apuntador del arreglo de caracteres con el que operaremos.
 * @param desde Es el índice inicial para hacer el algoritmo en el arreglo.
 * @param hasta Es el índice final de la región de elementos a aplicar con el algoritmo.
 */
void quickSort(char* elementos, int desde, int hasta){
    if (desde < hasta){
        int pibote = particion(elementos, desde, hasta);
        quickSort(elementos, desde, pibote - 1);
        quickSort(elementos, pibote + 1, hasta);
    }
}


int main(){
	char elementos[]={'l','h','t','a','w','z','y','d','o','e','x','p','r','a','c','l','k'};
	printf("Ejercicio 2\n");
	printDirection((char*)&elementos,13);
	quickSort((char*)&elementos,0,12);
	printDirection((char*)&elementos,13);
	return 0;
}


