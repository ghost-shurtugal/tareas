Ejercicio 3
----------------------------------------------------

## Documentación

/**
* @file practica1.c
* @brief Este archivo contiene el ejercicio 3 de la práctica 1.
*
* @author Josué Rodrigo Cárdenas Vallarta
*
* @date 14/02/2018
*/

## typedef char byte

/**
 * @brief Definición del tipo de dato byte.
 */

## int randomNumber()

/**
 * @brief Función que genera un entero de tamaño de un byte de manera aleatoria con base a lo que
 * 			se lee en /dev/random.
 *
 * @return El byte con formato de entero.
 */



