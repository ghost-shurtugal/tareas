/**
* @file practica1.c
* @brief Este archivo contiene el 3 de la práctica 1.
*
* @author Josué Rodrigo Cárdenas Vallarta
*
* @date 14/02/2018
*/

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

//Ejercicio 2

/**
 * @brief Definición del tipo de dato byte.
 */
typedef char byte;

/**
 * @brief Función que genera un entero de tamaño de un byte de manera aleatoria con base a lo que
 * 			se lee en /dev/random.
 *
 * @return El byte con formato de entero.
 */
int randomNumber(){
	byte result;
  	int idArchivo = open("/dev/random", O_RDONLY);
  	read(idArchivo, &result, sizeof(byte));
  	close(idArchivo);
  	return (int)result;
}


int main(){
	printf("Ejercicio 3\n");
	printf("%d\n", randomNumber());
	return 0;
}


