Ejercicio 1
----------------------------------------------------

## Documentación

/**
* @file practica1.c
* @brief Este archivo contiene el ejercicio 1 de la práctica 1.
*
* @author Josué Rodrigo Cárdenas Vallarta
*
* @date 14/02/2018
*/

## printFizzBuzz(int cantidadDeIteraciones)

/**
 * @brief Función que imprime una numeración, reemplazando números divisibles por 3 por "Fizz", números divisibles por 5 por "Buzz"
 *        y a los números que cumplan ambos por "FizzBuzz".
 *        Imprime el número en otro caso.
 *
 * @param cantidadDeIteraciones es el número de elementos a evaluar.
*/
