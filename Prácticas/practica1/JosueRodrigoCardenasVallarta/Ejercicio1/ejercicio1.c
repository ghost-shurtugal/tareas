/**
* @file practica1.c
* @brief Este archivo contiene el ejercicio 1 de la práctica 1.
*
* @author Josué Rodrigo Cárdenas Vallarta
*
* @date 19/02/2018
*/

#include <stdio.h>


//Ejercicio 1

/**
 * @brief Función que imprime una numeración, reemplazando números divisibles por 3 por "Fizz", números divisibles por 5 por "Buzz"
 *        y a los números que cumplan ambos por "FizzBuzz".
 *        Imprime el número en otro caso.
 *
 * @param cantidadDeIteraciones es el número de elementos a evaluar.
*/
void printFizzBuzz(int cantidadDeIteraciones){
	int n,b1,b2;
	for(n=1;n<=cantidadDeIteraciones;n++){
    b1=n%3==0;
    b2=n%5==0;
    switch(b1+b2){
      case 1:
        if(b1){
          printf("Fizz\n");
        }else{
          printf("Buzz\n");
        }
      break;
      case 2:
        printf("FizzBuzz\n");
      break;
      default:
        printf("%d\n", n);
    }
	}
}


int main(){
  printFizzBuzz(100);
	return 0;
}


