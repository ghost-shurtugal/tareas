Ejercicio 2
----------------------------------------------------

## Instalación y configuración de Roundcube
* Descargamos la paquetería necesaria del sitio oficial.
<p>
	<img src="img/1.png"/>
</p>

* La copiamos a la localización de nuestro servidor apache.
<p>
	<img src="img/2.png"/>
</p>
* Instalamos todas las dependencias que requiere Roundcube para funcionar (en este caso ya están instaladas).
<p>
	<img src="img/3.png"/>
</p>
* Roundcube necesita tener una base de datos con tablas específicas, para esto instalamos el manejador de bases de datos MariaDB (con configuración default e iniciando el servicio) para poder crear la base de datos de Roundcube que, posteriormente, configuraremos.
<p>
	<img src="img/5.png"/><br/>
	<img src="img/6.png"/>
</p>
* Una vez que hicimos estos previos podemos observar que en la página de instalación de Roundcube nos muestra los siguientes listados:
<p>
	<img src="img/7.png"/>
</p>
* Ocupamos el archivo default de configuración generado por Roundcube y lo editamos en la página:
<p>
	<img src="img/8.png"/>
</p>
* Por último configuramos bien a nuestro usuario y tenemos acceso a Roundcube.
<p>
	<img src="img/9.png"/><br/>
	<img src="img/10.png"/>
</p>
