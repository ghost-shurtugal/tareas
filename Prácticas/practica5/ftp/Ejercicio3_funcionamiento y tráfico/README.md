Ejercicio 3
----------------------------------------------------

## Funcionamiento y tráfico de FTP
* Se anexa un archivo que se genero con el comando <b>tcpdump -XAvi lo tcp port 21 | tee ejercicio3_ftp.log</b> , para monitorear el tráfico local por el puerto 21 (que es el que actualmente ftp ocupa).<br/>
  Se utiliza un funcionamiento similar a telnet, se autentifica sólo ocupando user y password sin fingerprint, se ejecutan los comandos y se regresa una respuesta con códigos del protocolo.
  Es importante remarcar que ftp no tiene encriptado, pasó la contraseña plana como lo podemos apreciar en la siguiente linea así como los comandos ejecutados y sus respuestas:<br/><br/>
  <p>
  11:43:32.366720 IP6 (hlim 64, next-header TCP (6) payload length: 48) localhost.35100 > localhost.ftp: Flags [P.], cksum 0x0038 (incorrect -> 0x59e7), seq 1:17, ack 21, win 342, options [nop,nop,TS val 4688672 ecr 4687452], length 16: FTP, length: 16<br/>
	<b>USER Shurtugal</b><br/>
  11:43:34.406136 IP6 (hlim 64, next-header TCP (6) payload length: 45) localhost.35100 > localhost.ftp: Flags [P.], cksum 0x0035 (incorrect -> 0xbfd3), seq 17:30, ack 55, win 342, options [nop,nop,TS val 4690711 ecr 4688672], length 13: FTP, length: 13<br/>
	<b>PASS prueba</b><br/>
  </p>
  <p>
  0x0040:  0047 a9d8 0047 a9d7 3235 3720 222f 686f  .G...G..257.<b>"/ho</b><br/>
  0x0050:  6d65 2f53 6875 7274 7567 616c 2f70 7275  <b>me/Shurtugal/pru</b><br/>
  0x0060:  6562 6173 2220 6372 6561 7465 640d 0a    <b>ebas".created..</b><br/>
  </p>
  
