Ejercicio 5
----------------------------------------------------

## FTPS
* FTPS es básciamente combinar FTP con SSL, por lo tanto necesitamos la siguiente paquetería (que ya tenemos instalada):
<p>
	<img src="img/1.png"/>
</p><br/>
* Acto seguido creamos la llave y el certificado para el servicio de ftps:
<p>
	<img src="img/2.png"/>
</p><br/>
* En el archivo de configuración de ftp (/etc/vsftpd) aregamos las siguientes lineas para habilitar ssl en el servicio ftp y reiniciamos el servicio después de editar el archivo:
<p>
	<img src="img/3.png"/><br/>
	<img src="img/4.png"/>
</p><br/>
* Intentamos conectarnos con ftp clásico pero ahora nos pide que usemos encriptamiento si queremos ocupar el servicio (como en el ejemplo no lo usamos cierra la conexión).
<p>
	<img src="img/5.png"/>
</p><br/>
* Para mostrar ftps utilizaremos filezilla, lo instalamos, abrimos, creamos la conexión (forzando TLS) y la abrimos.
<p>
	<img src="img/6.png"/><br/>
	<img src="img/7.png"/><br/>
	<img src="img/8.png"/>
</p><br/>
* Nos dirá si queremos confiar en el certificado que creamos anteriormente y le decimos que sí, al final podemos ver que se nos permite el acceso al servicio.
<p>
	<img src="img/9.png"/><br/>
	<img src="img/10.png"/>
</p><br/>
* En paralelo usamos el comando <b>tcpdump -XAvi lo tcp port 21 | tee ejercicio5_ftps.log</b> para hacer el monitoreo de tráfico y podemos observar en el log que todo está encriptado a diferencia de ftp donde lo pasaba todo plano.

## Diferencias entre FTPS y SFTP

* FTPS es utilizar FTP con certificados (SSL) para garantizar el encriptamiento y no mandar datos sensibles como contraseñas en texto plano.

* SFTP es un servicio basado en ssh que añade las características de FTP, es más robusto debido a que fue diseñado especificamente para manejar contenido cifrado.
