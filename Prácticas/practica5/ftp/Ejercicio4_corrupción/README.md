Ejercicio 4
----------------------------------------------------

## Uso de FTP para corrgeir archivos corruptos.
* Se hizo una transferencia de imágenes con ftp como se muestra en la sig. imagen:
<p>
	<img src="img/1.png"/>
</p><br/>
Se puede apreciar lo sig. :
 -  Se cambió a la lozalización del servidor donde se trabajaría.
 -  Se cambió a tipo de trasnferencia en ascii.
 -  Se hizo una copia de una imagen en ascii (el archivo se corrompió),
 -  Se cambio a tipo de transferencia binaria.
 -  Se copió el archivo corrupto en ascii con la trasnferencia binaria (siguió corrupto).
 -  Se hizo una copia del archivo original con trasnferencia binaria (no hubo corrupción).
Los resultados fueron los siguientes<br/>
<p>
	<img src="img/2.png"/>
</p>
