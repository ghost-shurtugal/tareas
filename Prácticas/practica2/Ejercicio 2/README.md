Ejercicio 2
----------------------------------------------------

## 1. Instalando servidor web

* Instalamos el servicio con el comando 'yum -y install httpd':

<p align="center">
  <img src="img/1.png"/><br/>
  <img src="img/2.png"/>
</p>

* Acto seguido habilitamos el servicio para que inicie al arrancar el equipo de cómputo, lo inicializamos para esta sesión y vemos su estado:

<p align="center">
  <img src="img/3.png"/>
</p>

* Entramos a localhost (que es lo mismo que poner la ip 127.0.0.1) por medio de un navegador para ver lo que corre en el servidor web:

<p align="center">
  <img src="img/4.png"/>
</p>

* Creamos el archivo index.html y lo colocamos en /var/www/html para que lo pueda leer el servidor web. Resolvemos un conflicto de SELinux y reiniciamos el servidor.<br/>
	Cuando ingresemos de nuevo por medio del navegador podremos ver el index actualizados.
<p align="center">
  <img src="img/5.png"/><br/>
  <img src="img/6.png"/>
</p>

* Para punto extra descargaremos a dos niveles la página de dgenp.unam.mx en /var/www/html/ con el comando 'wget -r -l 2 "dgenp.unam.mx"' <br/>
<p align="center">
  <img src="img/7.png"/><br/>
  <img src="img/8.png"/>
</p>

* Para el segundo punto extra agregamos las siguientes lineas en el archivo '/etc/httpd/conf/httpd.conf':
	RewriteEngine On<br/>
	RewriteCond %{REQUEST_METHOD} !^(GET|POST|HEAD)<br/>
	RewriteRule .* - [R=405,L]<br/>
 Podemos observar que los métodos que se especificaron en la pŕactica están desactivados exceptuando el GET, POST y HEAD:
<p align="center">
  <img src="img/9.png"/><br/>
  <img src="img/10.png"/>
</p>
