Ejercicio 1
----------------------------------------------------

## 1. Identificación de protocolos en captura de tráfico

* Se activó el comando para monitorear el tráfico en el puerto 80 y redireccionarse al archivo salida_ejercicio1.txt .

<p align="center">
  <img src="img/1.png"/>
</p>

* Acto seguido se visitó el sitio  http://dgenp.unam.mx, se obtuvieron los siguientes resultados:

<p align="center">
  <img src="img/2.png"/>
</p>

* Se ocupó el método GET con la versión 1.1 de HTTP
  En Hexadecimal:<br/>
  
	0x0000:  4500 01d2 d14e 4000 4006 d58f 0a00 020f  E....N@.@.......<br/>
	0x0010:  84f7 0142 a7e0 0050 acbf 340f 0a88 4a02  ...B...P..4...J.<br/>
	0x0020:  5018 7210 a711 0000 4745 5420 2f20 4854  P.r.....GET./.HT<br/>
	0x0030:  5450 2f31 2e31 0d0a 486f 7374 3a20 6467  TP/1.1..Host:.dg<br/>
	0x0040:  656e 702e 756e 616d 2e6d 780d 0a55 7365  enp.unam.mx..Use<br/>
	0x0050:  722d 4167 656e 743a 204d 6f7a 696c 6c61  r-Agent:.Mozilla<br/>
	0x0060:  2f35 2e30 2028 5831 313b 204c 696e 7578  /5.0.(X11;.Linux<br/>
	0x0070:  2078 3836 5f36 343b 2072 763a 3532 2e30  .x86_64;.rv:52.0<br/>
	0x0080:  2920 4765 636b 6f2f 3230 3130 3031 3031  ).Gecko/20100101<br/>
	0x0090:  2046 6972 6566 6f78 2f35 322e 300d 0a41  .Firefox/52.0..A<br/>
	0x00a0:  6363 6570 743a 2074 6578 742f 6874 6d6c  ccept:.text/html<br/>
	0x00b0:  2c61 7070 6c69 6361 7469 6f6e 2f78 6874  ,application/xht<br/>
	0x00c0:  6d6c 2b78 6d6c 2c61 7070 6c69 6361 7469  ml+xml,applicati<br/>
	0x00d0:  6f6e 2f78 6d6c 3b71 3d30 2e39 2c2a 2f2a  on/xml;q=0.9,*/*<br/>
	0x00e0:  3b71 3d30 2e38 0d0a 4163 6365 7074 2d4c  ;q=0.8..Accept-L<br/>
	0x00f0:  616e 6775 6167 653a 2065 6e2d 5553 2c65  anguage:.en-US,e<br/>
	0x0100:  6e3b 713d 302e 350d 0a41 6363 6570 742d  n;q=0.5..Accept-<br/>
	0x0110:  456e 636f 6469 6e67 3a20 677a 6970 2c20  Encoding:.gzip,.<br/>
	0x0120:  6465 666c 6174 650d 0a43 6f6e 6e65 6374  deflate..Connect<br/>
	0x0130:  696f 6e3a 206b 6565 702d 616c 6976 650d  ion:.keep-alive.<br/>
	0x0140:  0a55 7067 7261 6465 2d49 6e73 6563 7572  .Upgrade-Insecur<br/>
	0x0150:  652d 5265 7175 6573 7473 3a20 310d 0a49  e-Requests:.1..I<br/>
	0x0160:  662d 4d6f 6469 6669 6564 2d53 696e 6365  f-Modified-Since<br/>
	0x0170:  3a20 5375 6e2c 2032 3520 4665 6220 3230  :.Sun,.25.Feb.20<br/>
	0x0180:  3138 2030 303a 3332 3a33 3820 474d 540d  18.00:32:38.GMT.<br/>
	0x0190:  0a49 662d 4e6f 6e65 2d4d 6174 6368 3a20  .If-None-Match:.<br/>
	0x01a0:  2236 3731 312d 3536 3566 6538 3764 3232  "6711-565fe87d22<br/>
	0x01b0:  3661 3522 0d0a 4361 6368 652d 436f 6e74  6a5"..Cache-Cont<br/>
	0x01c0:  726f 6c3a 206d 6178 2d61 6765 3d30 0d0a  rol:.max-age=0..<br/>
	0x01d0:  0d0a

* Se activó el comando para monitorear el tráfico en el puerto 53 y redireccionarse al archivo salida_ejercicio1_dns.txt .

<p align="center">
  <img src="img/3.png"/>
</p>

* Acto seguido se visitó el sitio  http://dgenp.unam.mx, se obtuvieron los siguientes resultados:

<p align="center">
  <img src="img/4.png"/>
</p>
* Con el protocolo IP v4 y posteriormente con el ip v6 se resolvió el DNS.<br/>

  En Hexadecimal:
	23:13:08.407186 IP (tos 0x0, ttl 64, id 5496, offset 0, flags [DF], proto UDP (17), length 59)<br/>
    	redes.35474 > 10.0.2.3.domain: 35921+ A? dgenp.unam.mx. (31)<br/><br/>
	0x0000:  4500 003b 1578 4000 4011 0d29 0a00 020f  E..;.x@.@..)....<br/>
	0x0010:  0a00 0203 8a92 0035 0027 a4e9 8c51 0100  .......5.'...Q..<br/>
	0x0020:  0001 0000 0000 0000 0564 6765 6e70 0475  .........dgenp.u<br/>
	0x0030:  6e61 6d02 6d78 0000 0100 01              nam.mx.....<br/>
	23:13:08.407211 IP (tos 0x0, ttl 64, id 5497, offset 0, flags [DF], proto UDP (17), length 59)<br/>
	redes.35474 > 10.0.2.3.domain: 59016+ AAAA? dgenp.unam.mx. (31)<br/><br/>
	0x0000:  4500 003b 1579 4000 4011 0d28 0a00 020f  E..;.y@.@..(....<br/>
	0x0010:  0a00 0203 8a92 0035 0027 2fb2 e688 0100  .......5.'/.....<br/>
	0x0020:  0001 0000 0000 0000 0564 6765 6e70 0475  .........dgenp.u<br/>
	0x0030:  6e61 6d02 6d78 0000 1c00 01              nam.mx.....<br/>
