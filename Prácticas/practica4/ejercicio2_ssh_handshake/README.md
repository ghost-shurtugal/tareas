Ejercicio 2
----------------------------------------------------

## Handshake en ssh
Como podemos observar en la ejecución del comando <b>ssh -v 10.0.2.15 -p 22</b>:
<p>
	<img src="img/1.png"/><br/>
	<img src="img/2.png"/>
</p>

* Utilizando la configuración de la máquina cliente se inicia una conexión a la máquina host <b>10.0.2.15</b>, se mandan datos de indetificación del cleinte y el servidor le regresa su host key.
* En el ejercicio anterior ya se había almacenado su hostkey, por lo tanto se encuentra almacenada y la máquina cliente la compara con la que obtuvo en la primera conexión.
* Se manda la confirmación anterior al host y el host inicia a preguntar los datos de identificación del usuario que quiere utilizar el servicio.
* El usuario se identifica con su user name y su password y manda los datos al servidor.
* El servidor comprueba los datos y le da permiso al cliente de hacer una conexión al servicio.
<br/><br/>
* Estos pasos se asemejan al http debido a que en http el cliente manda sus datos de identificación al server, el server los valida y al final el cliente hace la conexión por medio de http. La diferencia radica que por default no es tan meticuloso en hacer varias interacciones y fases de autentificación.
