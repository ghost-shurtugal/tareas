Ejercicio 3
----------------------------------------------------

## 0. Instalando telnet:

* Ocupando yum instalamos la paquetería para ocupar telnet:

<p>
	<img src="img/1.png">
</p>

## 1. Utilizando ssh.

* Utilizando el comando <b>tcpdump -XAvi lo tcp port 22 | tee ejercicio3_ssh.log</b> para monitorear la actividad local por el puerto 22 y el comando <b>ssh Shurtugal@10.0.2.15 -p 22 "echo 'HOLA SSH'"</b>, haciendo que el host ejecute un echo que despliega la frase 'HOLA SSH': 
<p>
	<img src="img/2.png">
</p>
 Generamos el archivo ejercicio3_ssh.log, en el cuál apreciamos que viene cifrado y podemos intepretar que se manda la fingerprint del host codificada, en otras palabras, no sabemos a ciencia cierta qué enviamos con sólo ver este log.
<p>
<br/>
0xe479), seq 22:1518, ack 22, win 342, options [nop,nop,TS val 3172433 ecr 3172433], length 1496<br/>
	0x0000:  4500 060c 9d42 4000 4006 7f8c 0a00 020f  E....B@.@.......<br/>
	0x0010:  0a00 020f c2b8 0016 7953 f590 74da f254  ........yS..t..T<br/>
	0x0020:  8018 0156 1e1c 0000 0101 080a 0030 6851  ...V.........0hQ<br/>
	0x0030:  0030 6851 0000 05d4 0b14 2428 eeef cff4  .0hQ......$(....<br/>
	0x0040:  f535 cb1f 414d d14e 9687 0000 014b 6375  .5..AM.N.....Kcu<br/>
	0x0050:  7276 6532 3535 3139 2d73 6861 3235 362c  rve25519-sha256,<br/>
	0x0060:  6375 7276 6532 3535 3139 2d73 6861 3235  curve25519-sha25<br/>
	0x0070:  3640 6c69 6273 7368 2e6f 7267 2c65 6364  6@libssh.org,ecd<br/>
	0x0080:  682d 7368 6132 2d6e 6973 7470 3235 362c  h-sha2-nistp256,<br/>
	0x0090:  6563 6468 2d73 6861 322d 6e69 7374 7033  ecdh-sha2-nistp3<br/>
	0x00a0:  3834 2c65 6364 682d 7368 6132 2d6e 6973  84,ecdh-sha2-nis<br/>
	0x00b0:  7470 3532 312c 6469 6666 6965 2d68 656c  tp521,diffie-hel<br/>
	0x00c0:  6c6d 616e 2d67 726f 7570 2d65 7863 6861  lman-group-excha<br/>
	0x00d0:  6e67 652d 7368 6132 3536 2c64 6966 6669  nge-sha256,diffi<br/>
	0x00e0:  652d 6865 6c6c 6d61 6e2d 6772 6f75 7031  e-hellman-group1<br/>
	0x00f0:  362d 7368 6135 3132 2c64 6966 6669 652d  6-sha512,diffie-<br/>
	0x0100:  6865 6c6c 6d61 6e2d 6772 6f75 7031 382d  hellman-group18-<br/>
	0x0110:  7368 6135 3132 2c64 6966 6669 652d 6865  sha512,diffie-he<br/>
	0x0120:  6c6c 6d61 6e2d 6772 6f75 702d 6578 6368  llman-group-exch<br/>
	0x0130:  616e 6765 2d73 6861 312c 6469 6666 6965  ange-sha1,diffie<br/>
	0x0140:  2d68 656c 6c6d 616e 2d67 726f 7570 3134  -hellman-group14<br/>
	0x0150:  2d73 6861 3235 362c 6469 6666 6965 2d68  -sha256,diffie-h<br/>
	0x0160:  656c 6c6d 616e 2d67 726f 7570 3134 2d73  ellman-group14-s<br/>
	0x0170:  6861 312c 6469 6666 6965 2d68 656c 6c6d  ha1,diffie-hellm<br/>
	0x0180:  616e 2d67 726f 7570 312d 7368 6131 2c65  an-group1-sha1,e<br/>
	0x0190:  7874 2d69 6e66 6f2d 6300 0001 4765 6364  xt-info-c...Gecd<br/>
	0x01a0:  7361 2d73 6861 322d 6e69 7374 7032 3536  sa-sha2-nistp256 <br/>
...
</p>

## 2. Utilizando telnet

* Utilizando el comando <b>tcpdump -XAvi lo tcp port 23 | tee ejercicio3_ssh.log</b> para monitorear la actividad local por el puerto 23 y el comando <b>telnet 10.0.2.15 23</b>, intentando hacer una conexión similar a la de ssh pero sin comparar alguna fingerprint y sólo ocupando un login clásico con username y password.
<p>
	<img src="img/3.png">
</p>
 Ejecutamos otro echo con la leyenda 'HOLA TELNET' y salimos, revisamos el archivo ejercicio3_ssh.log y podemos ver que manda las cosas de manera plana.
<p>
<br/>
0x5676), seq 430:496, ack 232, win 350, options [nop,nop,TS val 3042373 ecr 3042371], length 66<br/>
	0x0000:  4510 0076 0af7 4000 4006 175e 0a00 020f  E..v..@.@..^....<br/>
	0x0010:  0a00 020f 0017 dc12 a9b8 4a64 609e fc97  ..........Jd`...<br/>
	0x0020:  8018 015e 1886 0000 0101 080a 002e 6c45  ...^..........lE<br/>
	0x0030:  002e 6c43 0d0a 484f 4c41 2054 454c 4e45  ..lC..<b>HOLA.TELNE</b><br/>
	0x0040:  540d 0a1b 5d30 3b53 6875 7274 7567 616c  <b>T...]0;Shurtugal</b><br/>
	0x0050:  406c 6f63 616c 686f 7374 3a7e 075b 5368  <b>@localhost:~.[Sh</b><br/>
	0x0060:  7572 7475 6761 6c40 6c6f 6361 6c68 6f73  urtugal@localhos<br/>
	0x0070:  7420 7e5d 2420                           t.~]$.<br/>
</p>
Apreciamos que manda todo lo que ejecutamos sin algún cifrado, incluso el prompt, no tiene la seguridad de ssh.

