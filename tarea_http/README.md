Tarea de HTTP
----------------------------------------------------

## Nombre

Cárdenas Vallarta Josué Rodrigo

### Correo electrónico

rodrigo.cardns@gmail.com

### Ejercicio 5

* No existen diferencias entre los archivos debido a que las cookies, al ser equivalentes, no afectan el contenido de la página (como sería el caso de una página sin autentificar comparada con la misma página pero con cookies de autentificación).

* En este caso sólo nos dice que se están añadiendo las cookies y en lugar de generarlas el servidor utiliza las que le pasamos de parámetro.

* Para que el servidor nos pueda devolver lo que queremos a partir de las cookies enviadas, como por ejemplo:
  - Que se adapte la página a nuestro navegador.
  - El idioma que manejamos.
  - Nuestro país de origen.
  - Etc.

* Porque si se mantenía esa opción se guardaba el archivo html en un archivo comprimido, en el caso de esta práctica no era muy funcional.

* Para que nos regresara un texto en formato html para que pudiéramos hacer la comparación con otro archivo html.

