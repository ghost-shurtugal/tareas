Documentación de los ejercicios.
----------------------------------------------------

### 1. Verificar que el dominio tenga el servicio de HTTP

El comando <b>nc -vz www.unam.mx 80</b> regresa la siguiente salida:
```
Ncat: Version 7.60 ( https://nmap.org/ncat )
Ncat: Connected to 198.185.159.144:80.
Ncat: 0 bytes sent, 0 bytes received in 0.25 seconds.
```
Por lo tanto, concluimos que sí tiene el servicio de HTTP.

### 2. Captura de tráfico

Se ocupo el siguiente comando para capturar el tráfico del sitio <b>www.pixar.com</b> por el puerto 80:
```
sudo tcpdump -i any -n -w www_pixar_com.pcap 'host www.pixar.com' and port 80
```
Se agregó el parámetro <b>-i any</b> para que por cualquier dispositivo de red se monitoreara y se añadió el filtro de <b> and port 80</b> para que sólo monitoreara 

### 3. Petición HTTP

La petición http que se solicitó con el comando <b>curl -v# 'http://www.pixar.com/' -o www_pixar_com.http.html 2>&1 | tr -d '\r' | tee www_pixar_com.http.log</b> fue la sig. :

```
$curl -v# 'http://www.pixar.com/' -o www_pixar_com.http.html 2>&1 | tr -d '\r' | tee www_pixar_com.http.log
*   Trying 198.49.23.145...
* TCP_NODELAY set
* Connected to www.pixar.com (198.49.23.145) port 80 (#0)
> GET / HTTP/1.1
> Host: www.pixar.com
> User-Agent: curl/7.55.1
> Accept: */*
> 
< HTTP/1.1 301 Moved Permanently
< Date: Sun, 15 Apr 2018 21:46:01 GMT
< X-ServedBy: web062
< Location: https://www.pixar.com/
< Transfer-Encoding: chunked
< x-contextid: C8pAlPS2/J1FoyXGx
< x-via: 1.1 echo026
< 
{ [5 bytes data]
* Connection #0 to host www.pixar.com left intact

```

### 4. Visualizando el resultado 

La versión en texto de la captura en el archivo <b>www_pixar_com.pcap.txt</b> contiene lo sig. :

```
reading from file www_pixar_com.pcap, link-type LINUX_SLL (Linux cooked)
    1  16:46:00.886202 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 76: (tos 0x0, ttl 64, id 12146, offset 0, flags [DF], proto TCP (6), length 60)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [S], cksum 0x9fe1 (incorrect -> 0xf3ae), seq 1195242701, win 29200, options [mss 1460,sackOK,TS val 2888987593 ecr 0,nop,wscale 7], length 0
    2  16:46:01.114446  In a8:ad:3d:e5:01:64 ethertype IPv4 (0x0800), length 76: (tos 0x0, ttl 53, id 0, offset 0, flags [DF], proto TCP (6), length 60)
    198.49.23.145.http > 192.168.1.72.58934: Flags [S.], cksum 0x43e1 (correct), seq 3024378512, ack 1195242702, win 25280, options [mss 1276,sackOK,TS val 3284135213 ecr 2888987593,nop,wscale 9], length 0
    3  16:46:01.114508 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 64, id 12147, offset 0, flags [DF], proto TCP (6), length 52)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [.], cksum 0x9fd9 (incorrect -> 0xd2ee), ack 1, win 229, options [nop,nop,TS val 2888987821 ecr 3284135213], length 0
    4  16:46:01.114638 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 145: (tos 0x0, ttl 64, id 12148, offset 0, flags [DF], proto TCP (6), length 129)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [P.], cksum 0xa026 (incorrect -> 0x84a0), seq 1:78, ack 1, win 229, options [nop,nop,TS val 2888987821 ecr 3284135213], length 77: HTTP, length: 77
	GET / HTTP/1.1
	Host: www.pixar.com
	User-Agent: curl/7.55.1
	Accept: */*
	
    5  16:46:01.319239  In a8:ad:3d:e5:01:64 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 53, id 46238, offset 0, flags [DF], proto TCP (6), length 52)
    198.49.23.145.http > 192.168.1.72.58934: Flags [.], cksum 0xd272 (correct), ack 78, win 50, options [nop,nop,TS val 3284135439 ecr 2888987821], length 0
    6  16:46:01.319267  In a8:ad:3d:e5:01:64 ethertype IPv4 (0x0800), length 273: (tos 0x0, ttl 53, id 46239, offset 0, flags [DF], proto TCP (6), length 257)
    198.49.23.145.http > 192.168.1.72.58934: Flags [P.], cksum 0x701c (correct), seq 1:206, ack 78, win 50, options [nop,nop,TS val 3284135445 ecr 2888987821], length 205: HTTP, length: 205
	HTTP/1.1 301 Moved Permanently
	Date: Sun, 15 Apr 2018 21:46:01 GMT
	X-ServedBy: web062
	Location: https://www.pixar.com/
	Transfer-Encoding: chunked
	x-contextid: C8pAlPS2/J1FoyXGx
	x-via: 1.1 echo026
	
    7  16:46:01.319288 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 64, id 12149, offset 0, flags [DF], proto TCP (6), length 52)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [.], cksum 0x9fd9 (incorrect -> 0xd017), ack 206, win 237, options [nop,nop,TS val 2888988026 ecr 3284135445], length 0
    8  16:46:01.319307  In a8:ad:3d:e5:01:64 ethertype IPv4 (0x0800), length 73: (tos 0x0, ttl 53, id 46240, offset 0, flags [DF], proto TCP (6), length 57)
    198.49.23.145.http > 192.168.1.72.58934: Flags [P.], cksum 0x8d73 (correct), seq 206:211, ack 78, win 50, options [nop,nop,TS val 3284135450 ecr 2888987821], length 5: HTTP
    9  16:46:01.319324 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 64, id 12150, offset 0, flags [DF], proto TCP (6), length 52)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [.], cksum 0x9fd9 (incorrect -> 0xd00d), ack 211, win 237, options [nop,nop,TS val 2888988026 ecr 3284135450], length 0
   10  16:46:01.319790 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 64, id 12151, offset 0, flags [DF], proto TCP (6), length 52)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [F.], cksum 0x9fd9 (incorrect -> 0xd00c), seq 78, ack 211, win 237, options [nop,nop,TS val 2888988026 ecr 3284135450], length 0
   11  16:46:01.449467  In a8:ad:3d:e5:01:64 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 53, id 46241, offset 0, flags [DF], proto TCP (6), length 52)
    198.49.23.145.http > 192.168.1.72.58934: Flags [F.], cksum 0xcff1 (correct), seq 211, ack 79, win 50, options [nop,nop,TS val 3284135663 ecr 2888988026], length 0
   12  16:46:01.449520 Out 9c:b6:d0:04:80:e3 ethertype IPv4 (0x0800), length 68: (tos 0x0, ttl 64, id 12152, offset 0, flags [DF], proto TCP (6), length 52)
    192.168.1.72.58934 > 198.49.23.145.http: Flags [.], cksum 0x9fd9 (incorrect -> 0xceb4), ack 212, win 237, options [nop,nop,TS val 2888988156 ecr 3284135663], length 0

```

Se transmitieron los siguientes doce paquetes:

* Esquema 

```
    Cliente         Servidor
       |               |
       |      SYN      |
1.     |-------------->|
       |               |
       |     SYN,ACK   |
2.     |<--------------|
       |               |
       |      ACK      |
3.     |-------------->|
       |               |
       |    GET/HTTP   |
4.     |-------------->|
       |               |
       |      ACK      |
5.     |<--------------|
       |               |
       |   HTTP/301    |
6.     |<--------------|
       |               |
       |      ACK      |
7.     |-------------->|
       |               |
       |   PUSH,ACK    |
8.     |<--------------|
       |               |
       |      ACK      |
9.     |-------------->|
       |               |
       |    FIN,ACK    |
10.    |-------------->|
       |               |
       |    FIN,ACK    |
11.    |<--------------|
       |               |
       |      ACK      |
12.    |-------------->|

```
* Descripción

Los primeros tres pasos conforman al famoso three way handshake para establecer una conexión, posterior a que tenemos la conexión el cliente solicita el sitio <b>www.pixar.com</b> y al final se cierra la conexión del paso 10 al 12.

1. El cliente manda una petición de syncronía (SYN) al servidor.
2. El servidor responde a la petición con una sincronía y un acknowledge (ACK).
3. El cliente devuelve un ACK confirmando que se recibió la confirmación de la sincronización al servidor.<br/><br/>
4. El usuario solicita la página www.pixar.com al puerto 80 al servidor.
5. El servidor responder a la solicitud con un ACK
6. El servidor regresa la página con código 301 al cliente.
7. El cliente confirma de recibido con un ACK.
8. El servidor manda un PUSH y un ACK al usuario.
9. El cliente confirma de recibido con un ACK.<br/><br/>
10. El usuario manda una petición FIN para terminar la conexión y un ACK.
11. El servidor responde a la petición con un FIN y un ACK.
12. El usuario confirma que recibió el FIN, ACK con un ACK y termina la conexión.

